#!/bin/bash

result=$( docker ps | grep backend )

if [[ -n "$result" ]]; then
  echo "Container exists"
  docker container rm -f backend
else
  echo "No such container"
fi

docker container run -d -p 8080:8080 --name backend nurwahid/backend-java:TAGS
